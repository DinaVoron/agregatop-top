﻿namespace WindowsFormsApp1
{
    partial class DeleteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DeleteTOPName = new System.Windows.Forms.TextBox();
            this.DeleteTOPAdress = new System.Windows.Forms.TextBox();
            this.DeleteOKButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Введите название ТОП";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Введите адрес ТОП";
            // 
            // DeleteTOPName
            // 
            this.DeleteTOPName.Location = new System.Drawing.Point(154, 33);
            this.DeleteTOPName.Name = "DeleteTOPName";
            this.DeleteTOPName.Size = new System.Drawing.Size(242, 20);
            this.DeleteTOPName.TabIndex = 2;
            // 
            // DeleteTOPAdress
            // 
            this.DeleteTOPAdress.Location = new System.Drawing.Point(154, 81);
            this.DeleteTOPAdress.Name = "DeleteTOPAdress";
            this.DeleteTOPAdress.Size = new System.Drawing.Size(242, 20);
            this.DeleteTOPAdress.TabIndex = 3;
            // 
            // DeleteOKButton
            // 
            this.DeleteOKButton.Location = new System.Drawing.Point(321, 124);
            this.DeleteOKButton.Name = "DeleteOKButton";
            this.DeleteOKButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteOKButton.TabIndex = 4;
            this.DeleteOKButton.Text = "OK";
            this.DeleteOKButton.UseVisualStyleBackColor = true;
            // 
            // DeleteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 166);
            this.Controls.Add(this.DeleteOKButton);
            this.Controls.Add(this.DeleteTOPAdress);
            this.Controls.Add(this.DeleteTOPName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DeleteForm";
            this.Text = "DeleteTOPForm";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox DeleteTOPName;
        private System.Windows.Forms.TextBox DeleteTOPAdress;
        private System.Windows.Forms.Button DeleteOKButton;
    }
}