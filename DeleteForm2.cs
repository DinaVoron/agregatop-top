﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public interface IDeleteForm2
    {
        void Show();
        string getTOPName();
        string getTOPAdress();
        string getDishName();
        void clean();
        event EventHandler DeleteDishOKButtonClick;
    }
    public partial class DeleteForm2 : Form, IDeleteForm2
    {
        string TOPname;
        string TOPadress;
        string DishName;

        public DeleteForm2()
        {
            InitializeComponent();
            DeleteDishOKButton.Click += DeleteDishOKButton_Click;
        }
        private void DeleteDishOKButton_Click(object sender, EventArgs e)
        {
            if (DeleteDishOKButtonClick != null) DeleteDishOKButtonClick(this, EventArgs.Empty);
        }
        public string getTOPName()
        {
            return deleteDishTOPName.Text;
        }
        public string getTOPAdress()
        {
            return deleteDishTOPAdress.Text;
        }
        public string getDishName()
        {
            return DeleteDishName.Text;
        }

        public void clean()
        {
            deleteDishTOPAdress.Text = "";
            deleteDishTOPName.Text = "";
            DeleteDishName.Text = "";
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        #region Проброска событий
        public event EventHandler DeleteDishOKButtonClick;
        #endregion
    }
}
