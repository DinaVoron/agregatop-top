﻿namespace WindowsFormsApp1
{
    partial class DeleteForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DeleteDishName = new System.Windows.Forms.TextBox();
            this.deleteDishTOPName = new System.Windows.Forms.TextBox();
            this.deleteDishTOPAdress = new System.Windows.Forms.TextBox();
            this.DeleteDishOKButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Введите название блюда";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Введите название ТОП";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Введите адрес ТОП";
            // 
            // DeleteDishName
            // 
            this.DeleteDishName.Location = new System.Drawing.Point(162, 22);
            this.DeleteDishName.Name = "DeleteDishName";
            this.DeleteDishName.Size = new System.Drawing.Size(242, 20);
            this.DeleteDishName.TabIndex = 5;
            // 
            // deleteDishTOPName
            // 
            this.deleteDishTOPName.Location = new System.Drawing.Point(162, 58);
            this.deleteDishTOPName.Name = "deleteDishTOPName";
            this.deleteDishTOPName.Size = new System.Drawing.Size(242, 20);
            this.deleteDishTOPName.TabIndex = 7;
            // 
            // deleteDishTOPAdress
            // 
            this.deleteDishTOPAdress.Location = new System.Drawing.Point(162, 92);
            this.deleteDishTOPAdress.Name = "deleteDishTOPAdress";
            this.deleteDishTOPAdress.Size = new System.Drawing.Size(242, 20);
            this.deleteDishTOPAdress.TabIndex = 8;
            // 
            // DeleteDishOKButton
            // 
            this.DeleteDishOKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteDishOKButton.Location = new System.Drawing.Point(329, 147);
            this.DeleteDishOKButton.Name = "DeleteDishOKButton";
            this.DeleteDishOKButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteDishOKButton.TabIndex = 9;
            this.DeleteDishOKButton.Text = "OK";
            this.DeleteDishOKButton.UseVisualStyleBackColor = true;
            // 
            // DeleteForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 182);
            this.Controls.Add(this.DeleteDishOKButton);
            this.Controls.Add(this.deleteDishTOPAdress);
            this.Controls.Add(this.deleteDishTOPName);
            this.Controls.Add(this.DeleteDishName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "DeleteForm2";
            this.Text = "DeleteForm2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox DeleteDishName;
        private System.Windows.Forms.TextBox deleteDishTOPName;
        private System.Windows.Forms.TextBox deleteDishTOPAdress;
        private System.Windows.Forms.Button DeleteDishOKButton;
    }
}