﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public interface IForm
    {
        string nameTOP { get; }
        string adressTOP { get; }
        event EventHandler DeleteButtonClick; //!реализовать
        event EventHandler AddButtonClick; //!реализовать
        event EventHandler SearchButtonClick;//!реализовать
        event EventHandler DeleteDishButtonClick;//!?
    }
    public partial class Form1 : Form, IForm
    {
        public Form1()
        {
            InitializeComponent();
            DeleteButton.Click += DeleteButton_Click;
            AddButton.Click += AddButton_Click;
            SearchButton.Click += SearchButton_Click;
            DeleteDishButton.Click += DeleteDishButton_Click;
        }

        private void DeleteDishButton_Click(object sender, EventArgs e)
        {
            if (DeleteDishButtonClick != null) DeleteDishButtonClick(this, EventArgs.Empty);
            //!throw new NotImplementedException();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (SearchButtonClick != null) SearchButtonClick(this, EventArgs.Empty);
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (AddButtonClick != null) AddButtonClick(this, EventArgs.Empty);
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            //!не поняла, что вообще сделала
            if (DeleteButtonClick != null) DeleteButtonClick(this, EventArgs.Empty);
        }

        #region Проброска событий
        public event EventHandler DeleteButtonClick; //!реализовать что???
        public event EventHandler AddButtonClick; //!реализовать
        public event EventHandler SearchButtonClick; //!реализовать
        public event EventHandler DeleteDishButtonClick; //!?
        #endregion
        #region Реализация IMain
        public string nameTOP
        {
            get { return ""; } //!реализовать 
        }
        public string adressTOP
        {
            get { return ""; } //!реализовать 
        }
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
        #endregion

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void DeleteButton_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }
    }
}

