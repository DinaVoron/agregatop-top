﻿using System.IO;
using AgregatorTOP.BL;

namespace WindowsFormsApp1
{
    public class MainPresenter
    {
        private readonly IDeleteForm _deleteView = new DeleteForm();
        private readonly IDeleteForm2 _deleteView2 = new DeleteForm2();
        private readonly IForm _view;
        private readonly IFileManager _manager;
        private readonly IMessageService _messageService;

        public MainPresenter(IForm view, IFileManager manager, IMessageService messageService)
        {
            _view = view;
            _manager = manager;
            _messageService = messageService;

            _view.DeleteButtonClick += _view_DeleteButtonClick;
            _view.AddButtonClick += _view_AddButtonClick;
            _view.SearchButtonClick += _view_SearchButtonClick;
            _view.DeleteDishButtonClick += _view_DeleteDishButtonClick;
            _deleteView.DeleteOKButtonClick += _deleteView_DeleteOKButtonClick;
            _deleteView2.DeleteDishOKButtonClick += _deleteView2_DeleteDishOKButtonClick;

        }

        private void _view_DeleteDishButtonClick(object sender, System.EventArgs e)
        {
            _deleteView2.Show();
        }

        private void _deleteView2_DeleteDishOKButtonClick(object sender, System.EventArgs e)
        {
            string dishName = _deleteView2.getDishName();
            string TOPName = _deleteView2.getTOPName();
            string TOPAdress = _deleteView2.getTOPAdress();
            _deleteView2.clean();
            if (!_manager.isDishExist(TOPName,TOPAdress,dishName))
            {
                _messageService.ShowMessage("Информации о данном блюде нет");
            }
            else
            {
                //!добавить вывод дерева
                _manager.deleteDish(TOPName, TOPAdress, dishName);
            }
        }

        private void _deleteView_DeleteOKButtonClick(object sender, System.EventArgs e)
        {
            //!реализовать
            string name = _deleteView.getDeleteTOPName();
            string adress = _deleteView.getDeleteTOPAdress();
            _deleteView.cleanDeleteTOPName();
            _deleteView.cleanDeleteTOPAdress();
            if (_manager.isTOPExist(name,adress))
            {
                //!реализовать
            }
            else
            {
                _messageService.ShowMessage("Информации о данной точке общественного питания нет");
            }
        }

        private void _view_SearchButtonClick(object sender, System.EventArgs e)
        {
            //!throw new System.NotImplementedException();
            _messageService.ShowMessage("Ищем");
            return;
        }

        private void _view_AddButtonClick(object sender, System.EventArgs e)
        {
            //!throw new System.NotImplementedException();
        }

        private void _view_DeleteButtonClick(object sender, System.EventArgs e)
        {
            _deleteView.Show();
            //передать презентору
            //df.get
            //if (_manager.isTOPExist())
            //!_manager.DeleteTOP();
        }
    }
}
