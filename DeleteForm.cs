﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public interface IDeleteForm
    {
        void Form2_Load(object sender, EventArgs e);
        void Show();
        string getDeleteTOPName();
        string getDeleteTOPAdress();
        void cleanDeleteTOPName(); //!сделать один клин
        void cleanDeleteTOPAdress();
        event EventHandler DeleteOKButtonClick; 
        
    }
    public partial class DeleteForm : Form, IDeleteForm
    {
        string sDeleteTOPName;
        string sDeleteTOPAdress;
        public DeleteForm()
        {
            InitializeComponent();
            DeleteOKButton.Click += DeleteOKButton_Click;
        }

        private void DeleteOKButton_Click(object sender, EventArgs e)
        {
            if (DeleteOKButtonClick != null) DeleteOKButtonClick(this, EventArgs.Empty);
        }

        public void Form2_Load(object sender, EventArgs e)
        {

        }

        public string getDeleteTOPName()
        {
            return DeleteTOPName.Text;
        }

        public string getDeleteTOPAdress()
        {
            return DeleteTOPAdress.Text;
        }

        public void cleanDeleteTOPName()
        {
            DeleteTOPName.Text = "";
        }

        public void cleanDeleteTOPAdress()
        {
            DeleteTOPAdress.Text = "";
        }

        #region Проброска событий
        public event EventHandler DeleteOKButtonClick;
        #endregion
    }
}
