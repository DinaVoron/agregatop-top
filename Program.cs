﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AgregatorTOP.BL;

namespace WindowsFormsApp1
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 form = new Form1();
            MessageService service = new MessageService();
            FileManager manager = new FileManager();
            MainPresenter presentor = new MainPresenter(form, manager, service);
            Application.Run(form);
        }
    }
}
